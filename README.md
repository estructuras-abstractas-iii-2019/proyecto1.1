# Proyecto 1, parte 1: Binary Search Tree

## Estudiantes:
```
Roberto Acevedo Mora
Alexander Calderón Torres
```

## Instrucciones de compilación:
Una vez descargado el código, ubíquese en la respectiva carpeta:
```
>>cd {PATH}/proyecto1.1
```
Seguidamente, ejecute el make:
```
>>make
```

## Para ejecutar el programa:

```
./bin/BST
```

## Para generar la documentación de Doxygen:
Ubíquese en la carpeta principal:
```
>>cd {PATH}/proyecto1.1
```

Compile el archivo Doxyfile:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd docs/latex
>>make
```
Esto generará el archivo refman.pdf con la documentación generada por doxygen.
