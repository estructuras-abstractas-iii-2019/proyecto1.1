#pragma once
/**
 * @author Alexander Calderón Torres
   @author Roberto Acevedo
 * @date 19/02/2020
 * @file nodo.h
 * @brief Aqui se define la estructura de cada nodo a utilizar.
 */
#include "nodo.h"

/**
 *  @class Tree
 *  @brief Clase emplantillada con el tipo Datum
 */
template <typename Datum> 
class Tree
{
public:

    /** 
     * @brief Constructor por defecto.
     */
	Tree();

    /** 
     * @brief Destructor por defecto.
     */	
	~Tree();
	
    /** 
     * @brief Método encargado de la inserción de nuevos elementos en el árbol
     */
	void insert(Nodo<Datum> *&arbol, Datum d, Nodo<Datum>* padre);

	
    /** 
     * @brief Método encargado de la eliminación de elementos en el árbol
     */
	void remover(Nodo<Datum>* arbol, Datum d);

	
    /** 
     * @brief Método encargado de la búsqueda de elementos en el árbol
     */	
	bool find(Nodo<Datum>* arbol, Datum d);

	
    /** 
     * @brief Método encargado de la búsqueda del elemento más grande en el subárbol izquierdo del árbol
     */	
	Datum findLargestToTheLeft(Nodo<Datum>* arbol);

	
    /** 
     * @brief Método encargado de la búsqueda del elemento más pequeño en el subárbol derecho del árbol
     */
	Datum findSmallestToTheRight(Nodo<Datum>* n);	
	

	
    /** 
     * @brief Método encargado de recorrer el árbol en preorden
     */
	void preOrden(Nodo<Datum>* arbol);

	
    /** 
     * @brief Método encargado recorrer el árbol en inorden
     */
	void inOrden(Nodo<Datum>* arbol);

	
    /** 
     * @brief Método encargado de recorrer el árbol en posorden
     */
	void posOrden(Nodo<Datum>* arbol);

	
    /** 
     * @brief Método auxiliar para la eliminación de un nodo
     */
	void deleteNodo(Nodo<Datum>* porBorrar);

	
    /** 
     * @brief Método encargado de reemplazar un nodo con otro. Funciona como auxiliar para la eliminación de nodos
     */
	void replaceNodo(Nodo<Datum>* arbol, Nodo<Datum>* reemplazo);


    /** 
     * @brief Método encargado de buscar el nodo mínimo a la izquierda de otro. Funciona como auxiliar para la eliminación de nodos
     */
	Nodo<Datum>* minimoIzquierdo(Nodo<Datum>* arbol);

    /** 
     * @brief Método encargado de crear un nuevo nodo. Funciona como auxiliar para la mayoría de funciones de la clase Tree 
     */
	Nodo<Datum>* crearNodo(Datum d, Nodo<Datum>* padre);

private:
    Datum predecesor = Datum();
    Datum sucesor = Datum(); 
};

#include "tree.tpp"