#pragma once
/**
 * @author Alexander Calderón Torres
   @author Roberto Acevedo
 * @date 19/02/2020
 * @file nodo.h
 * @brief Aqui se define la estructura de cada nodo a utilizar.
 */
#include <iostream>
#include <string>

/**
 *  @class Nodo
 *  @brief Clase emplantillada con el tipo Datum
 */
template <typename Datum> 
class Nodo
{
public:

    /** 
     * @brief Constructor por defecto.
     */
	Nodo();

	/** 
     * @brief Constructor por defecto.
     */
	~Nodo();

    /** 
     * @brief Método get para el dato almacenado en el nodo
     */
	Datum getDato();

    /** 
     * @brief Método get para el ancestro del nodo. Devuelve un puntero de tipo Nodo
     */	
     Nodo* getAncestro();

    /** 
     * @brief Método get para el hijo derecho del nodo. Devuelve una referencia a un puntero de tipo Nodo
     */	
	Nodo*& getHijoDer();


    /** 
     * @brief Método get para el hijo izquierdo del nodo. Devuelve una referencia a un puntero de tipo Nodo
     */	
	Nodo*& getHijoIzq();	


    /** 
     * @brief Método set para el ancestro del nodo
     * @param ancestro recibe un puntero de tipo Nodo al ancestro
     */	
	void setAncestro(Nodo* ancestro);


    /** 
     * @brief Método set para el hijo izquierdo del nodo
     * @param hijoIzq recibe un puntero de tipo Nodo al hijo izquierdo
     */	
	void setHijoIzq(Nodo* hijoIzq);

    /** 
     * @brief Método set para el hijo derecho del nodo
     * @param hijoDer recibe un puntero de tipo Nodo al hijo derecho
     */	
	void setHijoDer(Nodo* hijoDer);

    /** 
     * @brief Método set para el dato del nodo
     * @param d recibe un dato del tipo genérico Datum
     */		
	void setDato(Datum d);

private:
	Nodo* hijoIzq;
	Nodo* hijoDer;
	Nodo* ancestro;
	Datum dato;
};

#include "nodo.tpp"