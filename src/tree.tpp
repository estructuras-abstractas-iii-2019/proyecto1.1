#pragma once 
#include <iostream>
using namespace std;

template <typename Datum> 
Tree<Datum>::Tree(){}

template <typename Datum> 
Tree<Datum>::~Tree(){}

template <typename Datum> 
Nodo<Datum>* Tree<Datum>::crearNodo(Datum d, Nodo<Datum>* padre){

	Nodo<Datum>* nuevo = new Nodo<Datum>();
	nuevo -> setDato(d);
	nuevo -> setHijoIzq(NULL);
	nuevo -> setHijoDer(NULL);
	nuevo -> setAncestro(padre);

	return nuevo;
}

template <typename Datum> 
void Tree<Datum>::insert(Nodo<Datum> *&arbol, Datum d, Nodo<Datum>* padre){

	if (arbol==NULL){
		Nodo<Datum>* nuevo = crearNodo(d, padre);
		arbol = nuevo;
	} else {
		if (d < arbol -> getDato())
		{
			insert(arbol->getHijoIzq(), d, arbol);
		} else {
			insert(arbol -> getHijoDer(), d, arbol);
		}
	}
}

template <typename Datum> 
bool Tree<Datum>::find(Nodo<Datum>* arbol, Datum d){

	if (arbol==NULL){ return false;	} 

	else {
		if (d == arbol -> getDato()){ return true; } 
		else if (d < arbol -> getDato()) {	find(arbol -> getHijoIzq(),d);	} 
		else if (d > arbol -> getDato()) {	find(arbol -> getHijoDer(),d);	}
	}
}

template <typename Datum> 
void Tree<Datum>::preOrden(Nodo<Datum>* arbol){
	if (arbol==NULL){
		return;
	} else {
		cout << arbol->getDato() << " - ";
		preOrden(arbol->getHijoIzq());
		preOrden(arbol->getHijoDer());
	}
}

template <typename Datum> 
void Tree<Datum>::inOrden(Nodo<Datum>* arbol){
	if (arbol==NULL){
		return;
	} else {
		inOrden(arbol->getHijoIzq());
		cout << arbol->getDato() << " - ";
		inOrden(arbol->getHijoDer());
	}	
}

template <typename Datum> 
void Tree<Datum>::posOrden(Nodo<Datum>* arbol){
	if (arbol==NULL){
		return;
	} else {
		posOrden(arbol->getHijoIzq());
		posOrden(arbol->getHijoDer());
		cout << arbol->getDato() << " - ";
	}		
}

template <typename Datum> 
void Tree<Datum>::remover(Nodo<Datum>* arbol, Datum d){
	if (arbol==NULL){
		return;
	} else if (d<arbol->getDato()){
		remover(arbol->getHijoIzq(), d);
	} else if (d>arbol->getDato()){
		remover(arbol->getHijoDer(), d);
	} else {
		deleteNodo(arbol);
	}
}

template <typename Datum> 
void Tree<Datum>::deleteNodo(Nodo<Datum>* porBorrar){
	if (porBorrar->getHijoIzq() && porBorrar->getHijoDer())
	{
		Nodo<Datum>* masPequeno = minimoIzquierdo(porBorrar->getHijoDer());
		porBorrar->setDato(masPequeno->getDato());
		deleteNodo(masPequeno);  	
	} else if (porBorrar->getHijoIzq()){
		replaceNodo(porBorrar, porBorrar->getHijoIzq());
	} else if (porBorrar->getHijoDer()){
		replaceNodo(porBorrar, porBorrar->getHijoDer()); 
	} else {
		replaceNodo(porBorrar,NULL);
	}
}

template <typename Datum> 
void Tree<Datum>::replaceNodo(Nodo<Datum>* arbol, Nodo<Datum>* reemplazo){
	
	if (arbol->getAncestro()){
	
		if (arbol->getDato() == arbol->getAncestro()->getHijoIzq()->getDato()){
	
			arbol->getAncestro()->setHijoIzq(reemplazo);
	
		} else if (arbol->getDato() == arbol->getAncestro()->getHijoDer()->getDato()){

			arbol->getAncestro()->setHijoDer(reemplazo);
		}
	}

	if (reemplazo){	reemplazo -> setAncestro(arbol->getAncestro());}
}


template <typename Datum> 
Nodo<Datum>* Tree<Datum>::minimoIzquierdo(Nodo<Datum>* arbol){
	if (arbol==NULL){
		return NULL;
	}
	if (arbol->getHijoIzq()){
		return minimoIzquierdo(arbol);
	} else {
		return arbol;
	}
}

template <typename Datum> 
Datum Tree<Datum>::findLargestToTheLeft(Nodo<Datum>* arbol) 
{	
	if (arbol->getHijoIzq() != NULL) {
		
		Nodo<Datum>* temporal = arbol->getHijoIzq();
		
		while (temporal->getHijoDer() != NULL) {
			temporal = temporal->getHijoDer();
			this-> predecesor = temporal->getDato();
		}
	}
		return this->predecesor;
}

template <typename Datum> 
Datum Tree<Datum>::findSmallestToTheRight(Nodo<Datum>* arbol){
	
	if (arbol->getHijoDer() != NULL) {
		
		Nodo<Datum>* temp = arbol->getHijoDer();
		
		while (temp->getHijoIzq() != NULL) {
			temp = temp->getHijoIzq();
		}
			this -> sucesor = temp->getDato();
	} 
	return this->sucesor;
}