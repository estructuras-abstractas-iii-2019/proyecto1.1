#pragma once 

using namespace std;

template <typename Datum>
Nodo<Datum>::Nodo(){}

template <typename Datum>
Nodo<Datum>::~Nodo(){}

template <typename Datum>
Datum Nodo<Datum>::getDato(){return this->dato;}

template <typename Datum>
Nodo<Datum>* Nodo<Datum>::getAncestro(){return this->ancestro;}

template <typename Datum>
Nodo<Datum>*& Nodo<Datum>::getHijoDer(){return this->hijoDer;}

template <typename Datum>
Nodo<Datum>*& Nodo<Datum>::getHijoIzq(){return this->hijoIzq;}

template <typename Datum>
void Nodo<Datum>::setAncestro(Nodo<Datum>* ancestro){this->ancestro=ancestro;}

template <typename Datum>
void Nodo<Datum>::setHijoDer(Nodo<Datum>* hijoDer){this->hijoDer=hijoDer;}

template <typename Datum>
void Nodo<Datum>::setHijoIzq(Nodo<Datum>* hijoIzq){this->hijoIzq=hijoIzq;}

template <typename Datum>
void Nodo<Datum>::setDato(Datum d){this->dato=d;}