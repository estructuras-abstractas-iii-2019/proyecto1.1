#include "tree.h"

using namespace std;

int main(int argc, char const *argv[])
{
	int opcion;
	
	Nodo<int>* arbolito=NULL;
	Tree<int> tree;
	cout<<endl<<"Se ha creado un Binary Search Tree con éxito ! "<<endl;

	do{



		cout<<endl<< "Seleccione la acción a realizar en el árbol: " << endl<<endl;
		cout<<"1. Insertar un nuevo elemento"<<endl;
		cout<<"2. Buscar un elemento"<<endl;
		cout<<"3. Eliminar un elemento"<<endl;
		cout<<"4. Buscar el elemento más grande en el subárbol izquierdo"<<endl;
		cout<<"5. Buscar el elemento más pequeño en el subárbol derecho"<<endl;
		cout<<"6. Recorrer el árbol en Preorden"<<endl;
		cout<<"7. Recorrer el árbol en Inorden"<<endl;
		cout<<"8. Recorrer el árbol en Posorden"<<endl;	
		cout<<"9. < SALIR DEL PROGRAMA >"<<endl<<endl;
		cout<<">> ";	
		cin >> opcion;
		cout<<endl;
	
		switch(opcion){

			case 1:
			{
				int dato;
				cout<<"Digite el valor a insertar: ";
				cin >> dato;
				tree.insert(arbolito,dato,NULL);
				cout<<dato<<" insertado con éxito ! "<<endl<<endl;
			}

			break;
			case 2:
			{
				int dato;
				cout<<"Digite el valor a buscar en el árbol: ";
				cin >> dato;
				if(tree.find(arbolito,dato)){
					cout<<"El elemento "<<dato<<" sí forma parte del árbol"<<endl<<endl;
				} else {
					cout<<"El elemento "<<dato<<" no forma parte del árbol"<<endl<<endl;
				}
			}

			break;
			case 3:
			{
				int dato;
				cout<<"Digite el elemento que desea eliminar: ";
				cin >> dato;
				tree.remover(arbolito,dato);
				cout<<dato<<" eliminado con éxito ! "<<endl<<endl;
			}

			break;

			case 4:
			{
				cout<<"El elemento más grande a la izquierda de la raíz es:	";
				cout<<tree.findLargestToTheLeft(arbolito)<<endl<<endl;
			}

			break;
			case 5:
			{
				cout<<"El elemento más pequeño a la derecha de la raíz es:		";
				cout<<tree.findSmallestToTheRight(arbolito)<<endl<<endl;
			}

			break;
			case 6:
			{
				cout<<"El recorrido de todo el árbol en preorden es: "<<endl;
				tree.preOrden(arbolito);
				cout<<endl<<endl;
			}

			break;
			case 7:
			{
				cout<<"El recorrido de todo el árbol en inorden es: "<<endl;
				tree.inOrden(arbolito);
				cout<<endl<<endl;
			}

			break;
			case 8:
			{
				cout<<"El recorrido del árbol en posorden es: ";
				tree.posOrden(arbolito);
				cout<<endl<<endl;
			}

			break;

		}
	} while(opcion!=9);
	return 0;
}